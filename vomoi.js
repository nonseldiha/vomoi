"use strict";

class StringStream {
    constructor(s) {this.s=s, this.it=s[Symbol.iterator](); this.v="";}
    read() {if(!this.done){let nx=this.it.next(); nx.done?(this.done=true,this.v=undefined):this.v=nx.value};}
}

function skipSpaces(s) {while(s.v===' '&&!s.done)s.read();}
function readTillSpace(s) {let r=s.v; s.read(); while(s.v!==' '&&!s.done){r+=s.v,s.read()}; return r;}

function readToken(s) {skipSpaces(s); return readTillSpace(s);}
function parseNumber(t) {let n=+t;return isNaN(n)?undefined:n;}

// EXECUTION
// we read in "2 3 + ;"
// the semicolon compiles an anonymous word, puts it in IP and calls go()
// how does go work?
// it calls say() on the IP
// how do vomoi words work?
// they push themselves on the return stack
// the put the first word in IP
//
// forth words
// %esi (vm.nx) holds the next word to execute
// %eax (vm.ip) holds the word we jump to
// lodsl copies %esi into %eax and increments %esi by 4b (to point to next word)
// NEXT calls lodsl, then executes what's in eax
// DOCOL: push %esi on return stack, increment %eax, copy %eax to %esi, call NEXT
// EXIT:
//
// builtins end with a NEXT
// colons end with EXIT
// words in colons call each other through NEXT or they're builtin

function withRunning(wd,vm,tk){vm.rstack.push(wd);tk();vm.rstack.pop();}

function defWord(nm,go) {return {nm,go};}
function addWord(ns,wd) {ns.set(wd.nm,wd);}

const vomoiNS = new Map();

addWord(vomoiNS,defWord("DOCOL", (vm) => {vm.rstack.push(vm.nx);vm.nx=[vm.ip[0],vm.ip[1]+1];}));
addWord(vomoiNS,defWord("EXIT", (vm) => {vm.nx=vm.rstack.pop();}));
addWord(vomoiNS,defWord("+", (vm) => {vm.dstack.push(vm.dstack.pop()+vm.dstack.pop());}));

function lit(n) {return defWord("lit",(vm)=>vm.dstack.push(n));}
function baseNsMap() {let mp=new Map();mp.set('vomoi', vomoiNS);return mp;}

class VM {
    constructor() {
        this.rstack = [];
        this.dstack = [];
        this.nses = baseNsMap();
        this.ns = "vomoi";
    }

    findWord(word, ns = "vomoi") {
        this.nses.get(ns)?.get(word);
    }

    NEXT() {this.ip=[...this.nx];this.nx[1]++;this.ip[0][this.ip[1]].go(this);}

    run() {while(true)this.NEXT();}

    findWord(t) {return this.nses.get(this.ns)?.get(t);}
    parseToken(t) {return this.findWord(t)??lit(parseNumber(t));}

    readWords(s) {let ss=new StringStream(s),ws=[];while(!ss.done)ws.push(this.parseToken(readToken(ss)));return ws;}
    interpret(s) {this.nx=[this.readWords(s),0];this.run();}
}

let testvm = new VM();
// testvm.interpret("3 2 - pr");

// VM init from js
// require vomoi
// const vm = new VM();
// vm.loadFile(vomoiFile);
// vm.interpret("1 2 +");
//   alternatively
// vm.nx = [[vm.findWord("myword")],0];
// vm.run();
//   but why would anyone want to do that instead of
// vm.interpret("myword");
